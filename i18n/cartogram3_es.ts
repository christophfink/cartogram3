<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>Cartogram</name>
    <message>
        <location filename="../cartogram.py" line="109"/>
        <source>&amp;cartogram3</source>
        <translation>&amp;Cartograma</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="207"/>
        <source>Compute cartogram</source>
        <translation>Preparar cartograma</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="216"/>
        <source>&amp;Cartogram</source>
        <translation>&amp;Cartograma</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="435"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="268"/>
        <source>You need at least one polygon vector layer to create a cartogram.</source>
        <translation>Para preparar un cartograma se necesita al menos una capa vectorial de polígonos.</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="320"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="416"/>
        <source>cartogram3 successfully finished computing a cartogram for field â{fieldName}â after {iterations} iterations with {avgError:.2%} average error remaining.</source>
        <translation>cartogram3: Se terminó con exito la computación de un cartograma por el atributo «{fieldName}». Después {iterations} iteraciónes se queda un error promedio de {avgError:.2%}.</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="427"/>
        <source>cartogram3 computation cancelled by user</source>
        <translation>cartogram3: computación cancelada por usario</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="435"/>
        <source>An error occurred during cartogram creation. Please see the âPluginsâ section of the message log for details.</source>
        <translation>Ocurrió un error al ejecutar la computación.</translation>
    </message>
</context>
<context>
    <name>CartogramDialog</name>
    <message>
        <location filename="../cartogram_dialog.ui" line="14"/>
        <source>cartogram3</source>
        <translation>cartogram3</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="20"/>
        <source>Input layer:</source>
        <translation>Capa de entrada:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="27"/>
        <source>Value field(s):</source>
        <translation>Atributo(s):</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="41"/>
        <source>Stop conditions:</source>
        <translation>Metas:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="57"/>
        <source>max. number of iterations:</source>
        <translation>max. iteraciones:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="71"/>
        <source>max. average error:</source>
        <translation>error promedio max.:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="78"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="91"/>
        <source>Calculation stops as soon as one condition is met.</source>
        <translation>La computación se termina tan pronto como se satisfaga por lo menos una de las condiciones.</translation>
    </message>
</context>
<context>
    <name>CartogramWorker</name>
    <message>
        <location filename="../cartogram_worker.py" line="92"/>
        <source>Iteration {i}/{mI} for field â{fN}â</source>
        <translation>Iteración {i}/{mI} por atributo «{fN}»</translation>
    </message>
</context>
</TS>
