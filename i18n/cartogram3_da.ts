<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da" sourcelanguage="en">
<context>
    <name>Cartogram</name>
    <message>
        <location filename="../cartogram.py" line="109"/>
        <source>&amp;cartogram3</source>
        <translation>&amp;Cartogram</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="207"/>
        <source>Compute cartogram</source>
        <translation>Opret cartogram</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="216"/>
        <source>&amp;Cartogram</source>
        <translation>&amp;Cartogram</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="435"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="268"/>
        <source>You need at least one polygon vector layer to create a cartogram.</source>
        <translation>Du skal bruge mindst ét vektorlag for at kunne oprette et cartogram.</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="320"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="416"/>
        <source>cartogram3 successfully finished computing a cartogram for field â{fieldName}â after {iterations} iterations with {avgError:.2%} average error remaining.</source>
        <translation>cartogram3 succesfuldt afsluttet beregne en cartogram for felt ‚{fieldName}‘ efter {iterations} iterationer med en gennemsnitlig fejl af {avgError:.2%} tilbage.</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="427"/>
        <source>cartogram3 computation cancelled by user</source>
        <translation>cartogram3: oprettelse af cartogram stoppet af brugeren</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="435"/>
        <source>An error occurred during cartogram creation. Please see the âPluginsâ section of the message log for details.</source>
        <translation>Der opstod en fejl. Se venligst &amp;quot;Plugin&amp;quot;-loggen for flere detaljer.</translation>
    </message>
</context>
<context>
    <name>CartogramDialog</name>
    <message>
        <location filename="../cartogram_dialog.ui" line="14"/>
        <source>cartogram3</source>
        <translation>cartogram3</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="20"/>
        <source>Input layer:</source>
        <translation>Lag:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="27"/>
        <source>Value field(s):</source>
        <translation>Felt(er):</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="41"/>
        <source>Stop conditions:</source>
        <translation>Betingelser:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="57"/>
        <source>max. number of iterations:</source>
        <translation>max. iterationer:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="71"/>
        <source>max. average error:</source>
        <translation>max. gennemsnitlig fejl:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="78"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="91"/>
        <source>Calculation stops as soon as one condition is met.</source>
        <translation>Beregningen slutter, når en betingelse er opfyldt.</translation>
    </message>
</context>
<context>
    <name>CartogramWorker</name>
    <message>
        <location filename="../cartogram_worker.py" line="92"/>
        <source>Iteration {i}/{mI} for field â{fN}â</source>
        <translation>Iteration {i}/{mI} for felt ‚{fN}‘</translation>
    </message>
</context>
</TS>
