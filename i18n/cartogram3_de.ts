<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Cartogram</name>
    <message>
        <location filename="../cartogram.py" line="109"/>
        <source>&amp;cartogram3</source>
        <translation>&amp;Kartogramm</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="207"/>
        <source>Compute cartogram</source>
        <translation>Kartogramm berechnen</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="216"/>
        <source>&amp;Cartogram</source>
        <translation>&amp;Kartogramm</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="435"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="268"/>
        <source>You need at least one polygon vector layer to create a cartogram.</source>
        <translation>Für die Berechnung eines Kartogramms ist mindestens ein Polygon-Vektorlayer erforderlich.</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="320"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="416"/>
        <source>cartogram3 successfully finished computing a cartogram for field â{fieldName}â after {iterations} iterations with {avgError:.2%} average error remaining.</source>
        <translation>cartogram3: Kartogrammberechnung für Attribut ‚{fieldName}‘ nach {iterations} Durchgängen erfolgreich, verbleibender mittlerer Fehler: {avgError:.2%}.</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="427"/>
        <source>cartogram3 computation cancelled by user</source>
        <translation>cartogram3: Berechnung vom Benutzer abgebrochen</translation>
    </message>
    <message>
        <location filename="../cartogram.py" line="435"/>
        <source>An error occurred during cartogram creation. Please see the âPluginsâ section of the message log for details.</source>
        <translation>Fehler bei der Kartogrammberechnung. Einzelheiten im Fehlerprotokoll unter „Plugins“.
</translation>
    </message>
</context>
<context>
    <name>CartogramDialog</name>
    <message>
        <location filename="../cartogram_dialog.ui" line="14"/>
        <source>cartogram3</source>
        <translation>cartogram3</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="20"/>
        <source>Input layer:</source>
        <translation>Input Layer:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="27"/>
        <source>Value field(s):</source>
        <translation>Attribut(e):</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="41"/>
        <source>Stop conditions:</source>
        <translation>Bedingungen:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="57"/>
        <source>max. number of iterations:</source>
        <translation>max. Durchgänge:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="71"/>
        <source>max. average error:</source>
        <translation>max. mittlerer Fehler:</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="78"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../cartogram_dialog.ui" line="91"/>
        <source>Calculation stops as soon as one condition is met.</source>
        <translation>Die Berechnung endet, sobald eine Bedingung erfüllt ist.</translation>
    </message>
</context>
<context>
    <name>CartogramWorker</name>
    <message>
        <location filename="../cartogram_worker.py" line="92"/>
        <source>Iteration {i}/{mI} for field â{fN}â</source>
        <translation>Durchgang {i}/{mI} für Attribut ‚{fN}‘</translation>
    </message>
</context>
</TS>
